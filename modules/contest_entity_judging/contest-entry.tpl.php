<?php
/**
 * @file
 * contest-entry.tpl.php
 * 
 * 
 * When judging is active, on contests/[cid]/judging page, the 'view' link for
 * each entry is altered to AJAX-request the rendered entry - this template.
 * @see Drupal.behaviors.contest_entity_judging.rowToggle()
 * 
 * IMPORTANT: due to AJAX request path, this is rendered using the site theme.
 * *However* the contests/[cid]/judging page uses Bootstrap theme - so
 * relevant Bootstrap grid classes etc. may be used here.
 * Also, judging module's css applies to the page.
 * @see contest_entity_judging.css
 */

hide($content['links']);
hide($content['judging_form']);
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <h2><?php print t('Entry Details'); ?></h2>
    <div class="entry-fieldsets">
      <?php print render($content); ?>
      <div class="clearfix"></div>
    </div>
    <div class="row">
      <div class="col-sm-3 col-md-3 col-lg-3">
        <?php print render($content['links']); ?>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-6">
        <?php print render($content['judging_form']); ?>
      </div>
    </div>
  </div>
</div>
