<?php
/**
 * @file
 * contest_entity_judging.flag.inc
 * 
 * NOTE: "PFR" = pass/fail rework - converting pass/fail round to use scoring
 * (March 2015, Tory)
 */

/**
 * Contest_entry entity flag handler.
 */
class flag_contest_entry extends flag_entity {

  // Extend parent methods.
  function options() {

    $options = parent::options();
    $options += array(
      'is_queue' => TRUE,
      'is_queue_exclusive' => FALSE,
      'log_message_required' => FALSE,
      'description' => '',
    );

    if ($this->is_judging_flag()) {
      $options += array(
        'use_votingapi' => FALSE,
        'votingapi_votes_allowed' => 0,
        'voting_thank_you_content' => '',
        'voting_legal_content' => '',
        'voting_inactive_redirect_path' => '',
        
        'use_criteria' => TRUE,
        // PFR
        'pass_fail_scored' => FALSE,
        'criteria_count' => 0,
        'criteria_values' => '',
        'criteria_options_available' => 0,
        'criteria_description' => '',
        'finalize_per_entry' => TRUE,
      );
    }

    return $options;
  }

  function options_form(&$form) {

    parent::options_form($form);
    
    $form['description'] = array(
      '#type' => 'text_format',
      '#title' => t('@type description and help', array(
        '@type' => (!$this->is_judging_flag() ? 'Queue' : 'Round'),
      )),
      '#description' => t("Provide a description for this @action @type and any introductory helper text. This description will appear by default when an entry is viewed." . (isset($this->use_votingapi) && $this->use_votingapi ? "<br/>It will also be available for the site theme to render on the public-facing voting page." : ""), array(
        '@action' => (isset($this->use_votingapi) && $this->use_votingapi ? 'voting' : 'judging'),
        '@type' => (!$this->is_judging_flag() ? 'queue' : 'round'),
      )),
      '#default_value' => isset($this->description['value']) ? $this->description['value'] : '',
      '#format' => isset($this->description['format']) ? $this->description['format'] : NULL,
    );

    $form['contest_entity_judging'] = array(
      '#type' => 'fieldset',
      '#title' => t('Contest entry judging'),
      '#description' => '<p><strong>' . t('Once contest judging has begun, change these values only with great care. Data can be irreparably damaged otherwise.') . '</strong> ' . t('Export and import this flag, then edit to suit if values need to change between related/recurring contests.') . '</p>',
      '#collapsible' => FALSE,
      '#weight' => 10,
    );

    $form['contest_entity_judging']['log_message_required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Log message is required when a user sets this flag'),
      '#description' => t('The confirmation form display option will be configured when using this option.'),
      '#default_value' => isset($this->log_message_required) ? $this->log_message_required : FALSE,
    );

    if (!$this->is_judging_flag()) {
      $form['contest_entity_judging']['is_queue'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display as a judging queue'),
        '#default_value' => isset($this->is_queue) ? $this->is_queue : TRUE,
      );

      $form['contest_entity_judging']['is_queue_exclusive'] = array(
        '#type' => 'checkbox',
        '#title' => t('Entries will appear exclusively in this judging queue'),
        '#description' => t('Use this option to force entries to appear only in a single exclusive queue.<br /><em>Example:</em> If an entry is assigned to an exclusive "Needs Attention" queue, it can not also appear in an exclusive "Ineligible" queue. The entry will still appear in non-exclusive queues, such as a "Needs Printing" queue.'),
        '#default_value' => isset($this->is_queue_exclusive) ? $this->is_queue_exclusive : TRUE,
      );
    }
    else {
      $form['contest_entity_judging']['use_criteria'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use weighted judging criteria'),
        '#description' => t("If left unchecked this flag will serve as a simple pass/fail indicator. Otherwise, the configured scoring criteria form will be presented on the flag's confirmation form."),
        '#default_value' => isset($this->use_criteria) ? $this->use_criteria : TRUE,
      );
      
      // PFR
      $form['contest_entity_judging']['pass_fail_scored'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use scores to determine pass/fail'),
        '#description' => t("If enabled, entries will pass the round if their average score is 100%.<br />If enabled, each criterion will have 2 score options, corresponding to Pass and Fail."),
        '#default_value' => isset($this->pass_fail_scored) ? $this->pass_fail_scored : FALSE,
      );

      $judges = _contest_entity_judging_judges_query();
      $judges = $judges
        ->execute()
        ->fetchCol();

      $form['contest_entity_judging']['criteria_count'] = array(
        '#type' => 'select',
        '#title' => t('Number of required judging scores'),
        '#description' => t("If using weighted judging criteria, this is the number of users that are required to score an entry before it is eligible for advancement to the next round of judging. A user can only score an entry once."),
        '#options' => drupal_map_assoc(range(0, (!empty($judges) ? count($judges) : 5))),
        '#default_value' => isset($this->criteria_count) ? $this->criteria_count: 0,
      );

      $form['contest_entity_judging']['criteria_options_available'] = array(
        '#type' => 'select',
        '#title' => t('Number of criteria options available'),
        '#description' => t("Use this number to reduce the range of choices available to judges."),
        '#options' => drupal_map_assoc(array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
        '#default_value' => isset($this->criteria_options_available) ? $this->criteria_options_available : 0,
      );

      $form['contest_entity_judging']['criteria_options_available']['#options'][0] = t('default');

      $form['contest_entity_judging']['criteria_values'] = array(
        '#type' => 'textarea',
        '#title' => t('Weighted judging criteria'),
        '#description' => t("The possible values this field can contain. Enter one value per line, in the format key|label.<br />Key should be a numeric value and the sum of keys should equal 100. E.g. a key|label pair of 20|Creativity would mean the Creativity scoring criteria counts for 20% of the contest entry's total score."),
        '#default_value' => isset($this->criteria_values) ? $this->criteria_values : '',
      );

      $form['contest_entity_judging']['criteria_description'] = array(
        '#type' => 'text_format',
        '#title' => t('Judging criteria description'),
        '#description' => t("Provide a description for the criteria. This description will appear above the judging criteria form."),
        '#default_value' => isset($this->criteria_description['value']) ? $this->criteria_description['value'] : '',
        '#format' => isset($this->criteria_description['format']) ? $this->criteria_description['format'] : NULL,
      );
      
      $form['contest_entity_judging']['finalize_per_entry'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable checkbox for judges to finalize scores per entry'),
        '#description' => t("This includes a checkbox on the entry scoring form to allow judge to finalize their scores on that entry at any point.<br />If disabled, once they've completed scoring all assigned entries, they'll be able to finalize them all at once via the \"Submit All Scores\" button in the upper right."),
        '#default_value' => isset($this->finalize_per_entry) ? $this->finalize_per_entry : TRUE,
      );
    }
    

    if (module_exists('votingapi')) {
      $form['contest_entity_judging_voting'] = array(
        '#type' => 'fieldset',
        '#title' => t('Contest entry voting'),
        '#description' => '<p><strong>' . t('Once contest judging has begun, change these values only with great care. Data can be irreparably damaged otherwise.') . '</strong> ' . t('Export and import this flag, then edit to suit if values need to change between related/recurring contests.') . '</p>',
        '#collapsible' => FALSE,
        '#weight' => 11,
      );

      $form['contest_entity_judging_voting']['use_votingapi'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use !votingapi', array(
          '!votingapi' => l(t('Voting API'), 'admin/config/search/votingapi', array(
            'attributes' => array(
              'target' => '_blank',
            ),
          )),
        )),
        '#description' => t("If checked this judging round flag will be saved as Global and non-scored, and end-user voting will be enabled on contest entries that have this flag. Users must have the appropriate !permission bundle permissions in order to access the voting form.", array(
          '!permission' => '<em>' . l(t('Vote for judged contest entries'), 'admin/people/permissions', array(
            'fragment' => 'module-contest_entity_judging',
            'attributes' => array(
              'target' => '_blank',
            ),
          )) . '</em>',
        )),
        '#default_value' => isset($this->use_votingapi) ? $this->use_votingapi : FALSE,
      );
      
      // Use #states to show other voting-related settings only when voting is
      // enabled on this flag.
      $states_voting = array(
        'visible' => array(
          ':input[name="use_votingapi"]' => array('checked' => TRUE),
        ),
      );

      module_load_include('inc', 'votingapi', 'votingapi.admin');
      $votingapi_settings_form = drupal_get_form('votingapi_settings_form');

      $options_votingapi_votes_allowed = range(0, 10);
      $options_votingapi_votes_allowed = array_merge($options_votingapi_votes_allowed, array(20, 50, 100, 1000));
      $form['contest_entity_judging_voting']['votingapi_votes_allowed'] = array(
        '#type' => 'select',
        '#title' => t('Number of contest entry votes allowed'),
        '#description' => t("If using the Voting API, this is the number of votes allowed on all contest entries per user per the voting interval. The configured voting interval for anonymous users is @votingapi_anonymous_window; the configured voting interval for authenticated users is @votingapi_user_window.", array(
          '@votingapi_anonymous_window' => $votingapi_settings_form['votingapi_anonymous_window']['#options'][variable_get('votingapi_anonymous_window', 86400)],
          '@votingapi_user_window' => $votingapi_settings_form['votingapi_user_window']['#options'][variable_get('votingapi_user_window', -1)],
        )),
        '#options' => drupal_map_assoc($options_votingapi_votes_allowed),
        '#default_value' => isset($this->votingapi_votes_allowed) ? $this->votingapi_votes_allowed: 0,
        '#states' => $states_voting,
      );
      $form['contest_entity_judging_voting']['votingapi_votes_allowed']['#options'][0] = t('unlimited');
      
      $form['contest_entity_judging_voting']['voting_thank_you_content'] = array(
        '#type' => 'text_format',
        '#title' => t('Voting thank-you content'),
        '#description' => t("Enter content to display on voting page when user has already voted and cannot vote until the next interval."),
        '#default_value' => isset($this->voting_thank_you_content['value']) ? $this->voting_thank_you_content['value'] : '',
        '#format' => isset($this->voting_thank_you_content['format']) ? $this->voting_thank_you_content['format'] : NULL,
        '#states' => $states_voting,
      );
      
      $form['contest_entity_judging_voting']['voting_legal_content'] = array(
        '#type' => 'text_format',
        '#title' => t('Voting legal content'),
        '#description' => t("Enter a message to display on voting page to describe voting terms and conditions. (This will be available to optionally render in the template.)"),
        '#default_value' => isset($this->voting_legal_content['value']) ? $this->voting_legal_content['value'] : '',
        '#format' => isset($this->voting_legal_content['format']) ? $this->voting_legal_content['format'] : NULL,
        '#states' => $states_voting,
      );
      
      $form['contest_entity_judging_voting']['voting_inactive_redirect_path'] = array(
        '#type' => 'textfield',
        '#title' => t('Redirect path when voting is inactive'),
        '#description' => t('Enter a path to which the voting page will redirect if accessed when voting is not active on this contest.<br>Path should be relative to the site domain, and can be either a system path or alias. Examples: <code>node/7</code>, <code>contest-archive</code>.'),
        '#default_value' => isset($this->voting_inactive_redirect_path) ? $this->voting_inactive_redirect_path : '',
        '#states' => $states_voting,
      );
    }

  }
  
  function validate() {
    $errors = array();
    
    if ($this->use_votingapi && !empty($this->voting_inactive_redirect_path)) {
      $path = drupal_get_normal_path(ltrim($this->voting_inactive_redirect_path, '/'));
      $item = menu_get_item($path);
      if (empty($item)) {
        $errors['voting_inactive_redirect_path'][] = array(
          'error' => 'voting_inactive_path_nonexistent',
          'message' => t('Redirect path %path not found as a valid path on this site.', array(
            '%path' => $this->voting_inactive_redirect_path,
          )),
        );
      }
    }
      
    return $errors;
  }

  function save() {

    if ($this->is_judging_flag()) {
      $this->is_queue = FALSE;
      $this->is_queue_exclusive = FALSE;

      // Correct any misconfigured judging flag params.
      if (!empty($this->use_votingapi)) {
        $this->global = TRUE;
        $this->use_criteria = FALSE;
        $this->pass_fail_scored = FALSE;
        $this->criteria_count = 0;
        $this->criteria_options_available = 0;
        $this->criteria_values = '';
        $this->criteria_description = '';
      }
      elseif (!empty($this->use_criteria)) {
        $this->show_on_entity = 1;

        if ($this->link_type !== 'confirm') {
          $this->link_type = 'confirm';
          $this->flag_confirmation = $this->flag_message;
          $this->unflag_confirmation = $this->unflag_message;
        }

        if ($this->criteria_count == 0) {
          $this->criteria_count = 1;

          drupal_set_message(t('Number of required judging scores was automatically set to 1.'), 'warning');
        }
      }
      
      if (!empty($this->pass_fail_scored)) {
        $this->criteria_options_available = 2;
      }
    }

    if (!empty($this->log_message_required)) {
      $this->show_on_entity = 1;

      if ($this->link_type !== 'confirm') {
        $this->link_type = 'confirm';
        $this->flag_confirmation = $this->flag_message;
        $this->unflag_confirmation = $this->unflag_message;
      }
    }

    parent::save();
  }

  // Custom methods.
  function is_judging_flag() {
    if (property_exists($this, 'module')) {
      return ($this->module == 'contest_entity_judging');
    }

    return FALSE;
  }

  function get_previous_flag() {
    // Get previous flag based on weight.
    $prev = FALSE;
    $flags = flag_get_flags('contest_entry');
    foreach ($flags as &$flag) {
      if (!$flag->is_judging_flag()) {
        continue;
      }

      if ($this->name == $flag->name) {
        break;
      }
      else {
        $prev = $flag;
      }
    }

    if (!is_object($prev)) {
      $prev = FALSE;
    }

    return $prev;
  }
  
  /**
   * Return whether this flag represents a pass/fail round.
   * 
   * TODO: currently only applies to $this->global pass/fail. Incorporate
   * $this->pass_fail_scored case?
   */
  function is_pass_fail_round() {
    return ($this->is_judging_flag() && $this->global && empty($this->use_criteria) && empty($this->use_votingapi));
  }

  function uses_score_criteria() {
    return ((empty($this->use_votingapi) && !empty($this->use_criteria) && !empty($this->criteria_values) && ($this->criteria_count > 0)));
  }
  
  /**
   * For scored rounds, returns number of criteria which comprise overall score.
   * 
   * NOTE: this is different from $this->criteria_count, which is how many
   * judges must score the entry in this round.
   */
  function get_num_score_criteria() {
    if ($this->uses_score_criteria()) {
      // Pull string of weight|description values (one per line) and filter out
      // anything empty.
      $criteria = explode("\n", $this->criteria_values);
      $criteria = array_map('trim', $criteria);
      $criteria = array_filter($criteria, 'strlen');

      return count($criteria);
    }
    else {
      return 0;
    }
  }

  function get_score_criteria() {

    $options = array();

    if ($this->uses_score_criteria()) {
      $criteria = explode("\n", $this->criteria_values);
      $criteria = array_map('trim', $criteria);
      $criteria = array_filter($criteria, 'strlen');

      foreach ($criteria as $ix => $pair) {
        $parts = explode('|', $pair);
        $value = $value_base = $parts[0];
        $label = t($parts[1]) . " ($value_base%)";

        if (is_numeric($value)) {
          if (!empty($this->criteria_options_available)) {
            $value = $this->criteria_options_available;
          }

          // Set range of options, sort highest to lowest.
          $range = range(1, $value, 1);
          rsort($range);

          if (!empty($this->criteria_options_available)) {
            $first = array_slice($range, 0, 1);
            $first = $first[0];
            $options_altered = array();
            foreach ($range as $v) {
              
              // PFR
              if ($this->pass_fail_scored && $v == 1) {
                // For each criteria there are two score values corresponding
                // to pass and fail; make the lowest one 0.
                // Note that $this->criteria_options_available = 2 is enforced
                // on flag save.
                $v = 0;
              }
              
              // The weight of this criteria score towards the overall entry score
              $score_weight = (($v * $value_base) / $first);

              // MySQL trims/rounds to overall string length?
              // possibly a float length issue, not sure.
              // So some possible values are:
              //   16.6667
              //   15
              //   10
              //   8.33333
              if ($score_weight > 10) {
                $key = round($score_weight, 4);
              } else {
                $key = round($score_weight, 5);
              }
              
              // PFR
              if ($this->pass_fail_scored) {
                $v = ($v == 0 ? t("Fail") : t("Pass"));
              }

              $options_altered["{$key}"] = $v;
            }

            $options['criteria_' . $ix] = array(
              'label' => $label,
              'options' => $options_altered,
            );
          }
          else {
            $options['criteria_' . $ix] = array(
              'label' => $label,
              'options' => drupal_map_assoc($range),
            );
          }
        }
      }
    }

    return $options;
  }

  function get_count_queued($content_id) {
    // Suppress warning from flag if content for this flag is non-existent.
    $flag_content = @flag_get_content_flags('contest_entry', $content_id, $this->name);
    if (empty($flag_content)) {
      $flag_content = array();
    }

    return count($flag_content);
  }

  function get_count_scored($content_id) {

    $count = 0;
    if ($this->uses_score_criteria()) {
      // Get the judging criteria for this flag; we need to divide the count of
      // contest_entry_judging rows by the count of this array.
      $num_criteria = $this->get_num_score_criteria();

      // Scored flag; max of N criteria_count allowed per entity.
      // Get count of unique score sets.
      $computed_scored = db_select('contest_entry_judging', 'cej_scored')
        ->condition('fc_scored.fid', $this->fid)
        ->condition('cej_scored.ceid', $content_id)
        ->groupBy('cej_scored.ceid')
        ->groupBy('fc_scored.fid');
      $computed_scored
        ->addJoin('INNER', 'flag_content', 'fc_scored', "fc_scored.fcid = cej_scored.fcid");
      $computed_scored
        ->addExpression("CAST(COUNT(cej_scored.fcid) / " . $num_criteria . " AS UNSIGNED)", 'criteria_scored');
      $computed_scored = $computed_scored
        ->execute()
        ->fetchField(0);

      if (is_numeric($computed_scored)) {
        $count = $computed_scored;
      }
    }

    return $count;
  }

  function set_content_lock($content_id = array(), $uid = NULL, $lock = FALSE, $cron = FALSE) {

    // Unlock all flag_content for this user.
    if ($lock) {
      $this->set_content_lock(NULL, $uid, FALSE);
    }

    // Lock/unlock flag_content records per args.
    $update = db_update('flag_content')
      ->fields(array(
        'judging_uid_lock' => ($lock ? REQUEST_TIME : 0),
      ))
      ->condition('fid', $this->fid)
      ->condition('content_type', $this->content_type);
    if (!$lock && $cron) {
      $update->condition('judging_uid_lock', REQUEST_TIME - variable_get('contest_entity_judging_cron_unlock_interval', 7200), '<');
    }
    else {
      if (!empty($content_id)) {
        if (is_numeric($content_id)) {
          $content_id = array($content_id);
        }
        $update->condition('content_id', $content_id, 'IN');
      }
      if (!empty($uid)) {
        $update->condition('uid', $uid);
      }
    }

    return $update->execute();
  }

  function _votingapi_select_votes($content_id = NULL) {

    // Get supercookie instance.
    //global $_supercookie; // Supercookie v 7.x-1.5
    $_supercookie = supercookie_instance();
    // *IF* user already has a supercookie:
    // Run save() because it checks if the SC has expired, and will create/save
    // a new one if so. The new one will have a new scid, meaning user's votes
    // in the previous interval (previous SC) will not be associated to them,
    // and they will be free to vote again.
    // Note: only need to do this once in a request - the cookie is not going
    // to expire twice in a request. ;)
    if ($_supercookie->scid > 0) {
      static $sc_save_ran = FALSE;
      if (!$sc_save_ran) {
        $_supercookie = $_supercookie->save(REQUEST_TIME);
        $sc_save_ran = TRUE;
      }
    }


    // Set basic select criteria.
    $criteria = array(
      'entity_type' => 'contest_entry',
      'value_type' => 'vote',
      'value' => 1,
    );
    // Merge uid and vote_source to filter criteria.
    $criteria = array_merge($criteria, votingapi_current_user_identifier());
    // Use supercookie scid as vote_source, regardless of login status.
    $criteria['vote_source'] = $_supercookie->scid;
    
    if (!empty($content_id)) {
      $criteria['entity_id'] = $content_id;
    }

    $votes = votingapi_votingapi_storage_select_votes($criteria, $this->votingapi_votes_allowed);
    
    return $votes;
  }

  function votingapi_is_vote_allowed($content_id) {

    $allowed = FALSE;

    if ($this->use_votingapi && $this->get_count_queued($content_id) == 1) {

      if (!empty($this->votingapi_votes_allowed)) {
        // Limited number of votes allowed, across all entries.
        // Check total count of user's votes (within interval).
        // TODO? if total limit > 1, this allows multiple PER entry.
        $votes = $this->_votingapi_select_votes();
        $allowed = (count($votes) < $this->votingapi_votes_allowed);
      }
      else {
        // Unlimited votes allowed on all entries.
        // Check count of user's votes on this entry.
        // TODO? this limits to 1 PER entry (within interval).
        $votes = $this->_votingapi_select_votes($content_id);
        if (empty($votes)) {
          $allowed = TRUE;
        }
      }
    }

    return $allowed;
  }

  function votingapi_set_vote($content_id) {

    global $user;
    //global $_supercookie; // Supercookie v 7.x-1.5
    $_supercookie = supercookie_instance();

    $vote_source = $_supercookie->scid;
    if (!empty($vote_source)) {
      $vote = array(
        'entity_type' => 'contest_entry',
        'entity_id' => $content_id,
        'value_type' => 'vote',
        'tag' => 'vote',
        'uid' => $user->uid,
        'value' => 1,
        'timestamp' => REQUEST_TIME,
        'vote_source' => $vote_source,
      );

      // Save vote and update cookie expiration.
      //votingapi_set_votes($votes);
      votingapi_votingapi_storage_add_vote($vote);
      
      // $_supercookie->touch(); // Supercookie v 7.x-1.5
      // save() seems to be appropriate replacement for touch(). If the SC is
      // expired, it will delete old SC and save a new one, with `expires`
      // updated to the interval after current timestamp. If the SC is NOT yet
      // expired, ultimately it will just update its `modified` timestamp.
      // Also note: save() needs a $timestamp arg, although it promptly
      // overwrites it to be REQUEST_TIME. Just pass the same to avoid php
      // warning.
      $_supercookie->save(REQUEST_TIME);
      
      // Log successful vote.
      $audit_trail = array_merge(_contest_entity_judging_de_audit_trail_type_args($this), array(
        'action_name' => 'vote-submitted',
        'action_label' => t("Vote submitted"),
        'action_description' => t("A user submitted a vote for a contest entry"),
        // Note: uid, ip, and timestamp are logged already.
        'payload' => array(
          'supercookie_id' => $vote_source,
          'entry_id' => $content_id,
        ),
      ));
      de_audit_trail($audit_trail);

      return TRUE;
    }

    return FALSE;
  }

}
