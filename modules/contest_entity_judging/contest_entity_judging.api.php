<?php
/**
 * @file
 * contest_entity_judging.api.php
 */

/**
 * Alter the query that returns entries for a queue.
 */
function hook_query_CONTEST_ENTITY_JUDGING_FLAG_QUEUE_alter(QueryAlterableInterface &$query) {

}
/**
 * Alter the query that returns judges.
 */
function hook_query_CONTEST_ENTITY_JUDGING_JUDGES_alter(QueryAlterableInterface &$query) {

}
/**
 * Alter the query that determines the list of judges that are available for
 * assignments in a particular scored judging round.
 */
function hook_query_CONTEST_ENTITY_JUDGING_JUDGES_RANDOM_POOL_alter(QueryAlterableInterface &$query) {

}
/**
 * Alter the query that returns log entries to an entry detail.
 */
function hook_query_CONTEST_ENTITY_JUDGING_LOG_alter(QueryAlterableInterface &$query) {

}
/**
 * Alter the random assignment of judges to an entry in a scoring round.
 */
function hook_contest_entity_judging_judges_rand_assign_alter(&$empty1, &$empty2, &$variables) {

  $judges_rand = &$variables['judges_rand'];
  $judges_round = &$variables['judges_round'];
  $entry = &$variables['contest_entry'];
  $flag = &$variables['flag'];
  $messages = &$variables['messages'];

}
/**
 * This hook is invoked when all judges assigned to an entry in a round
 * have submitted their final scores.
 */
function hook_contest_entity_judging_judges_score_final($entry, $criteria, $flag) {

}
