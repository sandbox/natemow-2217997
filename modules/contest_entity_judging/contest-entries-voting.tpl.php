<?php
/**
 * @file
 * contest-entries-voting.tpl.php
 * 
 * Main variables:
 * - $can_vote_currently: boolean whether user is currently allowed to vote on
 *   any of the entries displayed.
 * - $already_voted: boolean whether user has already voted on any of the
 *   entries displayed.
 * - $round_description: formatted HTML to introduce/describe this round. This
 *   is the "Round description and help" field on the flag, if not empty.
 * - $voting_thanks: formatted HTML to display when user has already voted /
 *   cannot currently vote in this interval. It's the "Voting thank-you content"
 *   field on the flag.
 * - $legal_message: formatted HTML to describe terms and conditions of voting.
 *   This is the "Voting legal content" field on the flag.
 * - $entries: array of render arrays of contest entries, in 'teaser' view mode.
 *   They can be rendered all at once or looped to add wrapper markup to each
 *   entry.
 * 
 * NOTE: the voting form is injected into each entry teaser's render array; if
 * using a custom template for entries, it must be rendered there.
 * @see contest_entity_judging_entity_view_alter()
 * 
 * @see template_preprocess_contest_entries_voting()
 */
?>
<?php if ($can_vote_currently): ?>
  <?php if (!empty($round_description)): ?>
    <div class="description">
      <?php print $round_description; ?>
    </div>
  <?php endif; ?>

<?php elseif ($already_voted): ?>
  <?php if (!empty($voting_thanks)): ?>
    <div class="description">
      <?php print($voting_thanks); ?>
    </div>
  <?php endif; ?>

<?php else: 
  // TODO? I don't think this case ever occurs. Users w/o vote perm cannot
  // access the page.
  ?>
  <?php print t("Sorry, you do not have permission to vote."); ?>
<?php endif; ?>

<ul class="<?php print $classes; ?>">
  <?php foreach ($entries as $entry): ?>
    <li><?php print render($entry); ?></li>
  <?php endforeach; ?>
</ul>

<?php if (!empty($legal_message)): ?>
  <div class="voting-legal">
    <?php print $legal_message; ?>
  </div>
<?php endif; ?>
