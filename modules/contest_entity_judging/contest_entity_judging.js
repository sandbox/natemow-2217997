/**
 * @file
 * contest_entity_judging.js
 */

(function ($) {

  $.fn.contest_entity_judging_attachBehaviors = function() {
    Drupal.attachBehaviors($(this));
  };

  $.fn.scrollQueueTable = function() {
    var $self = $(this);
    $('html, body').animate({
      scrollTop: $self.offset().top
    }, 500);

    return $self;
  };

  Drupal.progressBar.prototype.stopMonitoring = function () {
    clearTimeout(this.timer);
    this.uri = null;

    // Force hard page refresh when this progressBar completes so user sees
    // session confirmation messages.
    if (this.id == 'ajax-progress-confirmed_bulk_operation_assign' || this.id == 'ajax-progress-confirmed_bulk_operation_delete' || this.id == 'ajax-progress-confirmed_bulk_operation_generate') {
      window.location.reload(true);
    }
  };
    
  Drupal.behaviors.contest_entity_judging = {
    attach: function(context, settings) {

      Drupal.behaviors.contest_entity_judging.loadScoreDetails(context, settings);

      // Judging is active if contest mode is set to offline.
      if (settings.contest_entity_judging.active) {
        Drupal.behaviors.contest_entity_judging.bulkOperationForm(context, settings);
        Drupal.behaviors.contest_entity_judging.setTabCounts(context, settings);
        Drupal.behaviors.contest_entity_judging.rowToggle(context, settings);
        Drupal.behaviors.contest_entity_judging.confirmationToggle(context, settings);
        Drupal.behaviors.contest_entity_judging.fieldsetToggle(context, settings);
        Drupal.behaviors.contest_entity_judging.judgeAssignCountLimit(context, settings);
        Drupal.behaviors.contest_entity_judging.labelEmptyEntryForms(context, settings);
      }

    },
    bulkOperationForm: function(context, settings) {
      $('form.judge-assignment .confirmation-actions a.cancel', context)
        .click(function(evt) {
          evt.preventDefault();
          $('form.judge-assignment #bulk-confirmation, form.judge-assignment .confirmation-actions')
            .hide();
          $('form.judge-assignment .form-actions > .form-submit')
            .show();
        });

      var $form = $('form.judge-assignment');
      if ($form.length) {

        $('#bulk-confirmation, .confirmation-actions .form-submit', $form)
          .mousedown(function() {
            var $self = $(this);
            $self
              .addClass('disabled');
            $self
              .parents('.confirmation-actions')
              .find('a.cancel')
              .hide();
          });

        $('select[name="round"]', $form)
          .change(function() {
            var $self = $(this);
            var $btn = $('.form-submit:not(".confirmation-actions .form-submit")', $form);

            if ($self.hasClass('progress-disabled')) {
              $btn
                .addClass('disabled');              
            }
          });

        if (settings.contest_entity_judging.bulk_confirmation) {
          $('#bulk-confirmation, .confirmation-actions.' + settings.contest_entity_judging.operation, $form)
            .show();
          $('.form-actions > .form-submit', $form)
            .hide();
        }
        else {
          $('#bulk-confirmation, .confirmation-actions', $form)
            .hide();
          $('.form-actions > .form-submit', $form)
            .show()
            .removeClass('disabled');

          if (settings.contest_entity_judging.flag_round_active) {
            if ($('select[name="round"]', $form).val() !== settings.contest_entity_judging.flag_round_active) {
              $('.form-actions > #edit-assign, .form-actions > #edit-generate', $form)
                .fadeOut();
            }
            else {
              $('.form-actions > #edit-assign, .form-actions > #edit-generate', $form)
                .hide()
                .fadeIn();
            }
          }
        }
      }

    },
    setTabCounts: function(context, settings) {

      var $tab_counts = $('ul.tabs.primary a > span.count, ul.tabs.secondary a > span.count, ul.nav-tabs.tabs--primary a > span.count', $('.tabs-judging'));
      if ($tab_counts.length) {
        $tab_counts.each(function() {
          var $self = $(this);

          $.ajax({
            type : 'GET',
            url : $self.parents('a').prop('href') + '/count',
            complete : function(xhr) {
              var response = JSON.parse(xhr.responseText);
              $self.text('(' + response.count + ')');
            }});

        });
      }

    },
    // On contests/[cid]/judging, alter 'view' link for each entry to AJAX-load
    // the rendered entry and append. Rendered entry uses contest-entry.tpl.php,
    // using the site's front-end theme.
    rowToggle: function(context, settings) {
      
      var table_selector = 'body.page-contests-judging table.contest-entity-admin-contest-entry-results';

      // Force any file element links to open in a new window.
      $(table_selector + ' .field-type-file .file a')
        .prop('target', '_blank');

      // Expand an entry row for review.
      $(table_selector + ' ul.links li.view a', context)
        .click(function(evt) {
          evt.preventDefault();
          
          var $self = $(this);
          var $table = $self.parents('table');
          
          // If there is another row currently expanded in the table, don't
          // open this one. User must close that row first, so that it'll be
          // updated on front end to show scoring status.
          if (!$self.hasClass('expanded') && $table.find('tr.cloned').length) {
            return;
          }
          
          // If expanding/collapsing any row is in progress, do nothing.
          if ($table.find('td ul.links li.view a.progress-disabled').length) {
            return;
          }
          
          // Disable link while JS/AJAX is in progress.
          $self.addClass('progress-disabled');
          // Use the Bootstrap throbber.
          $self.before('<div class="ajax-progress ajax-progress-throbber"><i class="glyphicon glyphicon-refresh glyphicon-spin"></i></div>');
          
          
          // Expand entry.
          if (!$self.hasClass('expanded')) {
            // If there is another row currently expanded in the table, don't
            // open this one. User must close that row first, so that it'll be
            // updated on front end to show scoring status.
            if ($table.find('tr.cloned').length) {
              return;
            }
            
            // Get the rendered entry in full view mode via AJAX request to its
            // 'view' url. Append to cloned table row just below the original.
            $.ajax({
              type : 'GET',
              // Attach URL param to denote this judging context.
              // This helps with theming, since the request ends up using site's
              // front-end theme and full view mode, i.e. same as full-page view
              // of the entity.
              url : $self.prop('href') + '&js=1&judging=1',
              complete : function(xhr) {
                var response = JSON.parse(xhr.responseText);

                if (xhr.status === 200) {
                  var $source = $self
                    .parents('tr')
                    .addClass('no-border active')
                    .fadeTo('fast', 1);

                  response = $(document.createElement('div'))
                    .append(response)
                    .addClass('active')
                    .hide()
                    .fadeIn();

                  var $target = $source
                    .clone()
                    .empty()
                    .append(
                      $(document.createElement('td'))
                        .prop('colspan', $source.find('td').length)
                        .append(response)
                    )
                    .prop('class', $source.prop('class') + ' cloned')
                    .removeClass('no-border')
                    .prop('style', 'display: table-row;')
                    .insertAfter($source)
                    .fadeTo('fast', 1);

                  $source
                    .scrollQueueTable();
                  $self.parents('table')
                    .find('> tbody > tr')
                    .not($source)
                    .not($target)
                    .fadeTo('fast', .20);

                  // Reattach behaviors to newly AJAX'd content.
                  Drupal.attachBehaviors(response, settings);
                }

                $self.prev('div.ajax-progress').remove();
                $self
                  .removeClass('progress-disabled')
                  .addClass('expanded')
                  .text(Drupal.t('close'));
              }
            });
          }
          // Collapse entry; update row on front-end with scoring status.
          else {
            var $tr = $self.parents('tr.active');
            var $trCloned = $tr.next('tr.active.cloned');
            var flagFormData = $trCloned.data();
            var $formErrors = $trCloned.find('form[id^="flag-confirm"] .form-item.error');
            
            // Fade in other rows.
            $('tr', $table).fadeTo('fast', 1);
            
            // Deactivate this row.
            $tr.removeClass('no-border').removeClass('active');
            $trCloned.remove();
            
            // If form was not canceled/closed with errors, update row to
            // indicate newly-submitted scoring (front-end only).
            if ($formErrors.length === 0) {

              // Update <tr> classes to show whether scored/finalized.
              if (flagFormData['scored'] !== undefined) {
                if (flagFormData['scored']) { $tr.addClass('scored'); }
                else { $tr.removeClass('scored'); }
              }

              if (flagFormData['finalized'] !== undefined) {
                if (flagFormData['finalized']) { $tr.addClass('final'); }
                else { $tr.removeClass('final'); }
              }

              // Update labels or numbers in span.judging-status (except % 
              // for admin).
              // @see contest_entity_judging_form_contest_entity_admin_contest_entry_alter()
              var $spanCtr = $tr.find('td span.judging-status');

              // -- Pass/fail labels.
              if (flagFormData['passed'] !== undefined) {
                var $label = $spanCtr.find('.pass-fail');
                if ($label.data('original-text') === undefined) {
                  $label.data('original-text', $label.text());
                }

                if (flagFormData['scored']) {
                  if (flagFormData['passed']) { $label.text('pass').addClass('ok').removeClass('error'); }
                  else { $label.text('fail').addClass('error').removeClass('ok'); }
                }
                else {
                  $label.text($label.data('original-text')).addClass('ok').removeClass('error');
                }
              }
              // -- X / Y scores
              // @see line ~2435
              else {
                if (flagFormData['scored'] !== undefined) {
                  var $count = $spanCtr.find('.count-scored');
                  var num = parseInt($count.text());
                  var total = parseInt($spanCtr.find('.count-queued').text());

                  // Only increment if user has *added* their score; only
                  // decrement if user has *removed* their score.
                  if (!flagFormData['currentlyScored'] && flagFormData['scored']) {
                    if (num < total) {
                      num++;
                      $count.text(num);
                      if (num === total) {
                        $count.addClass('ok').removeClass('error');
                      }
                    }
                  }
                  else if (flagFormData['currentlyScored'] && !flagFormData['scored']) {
                    num--;
                    if (num >= 0) {
                      $count.text(num);
                      $count.addClass('error').removeClass('ok');
                    }
                  }
                }
              }
            }

            $self.prev('div.ajax-progress').remove();
            $self.removeClass('expanded')
              .text(Drupal.t('view'))
              .removeClass('progress-disabled');
          }
        });
        
        
        // Before form submit, stash form status stuff for use on updating
        // the entry row when it's collapsed.
        var $flagForm = $('form[id^="flag-confirm"]', context).not('.submit-stash-processed');
        if ($flagForm.length) {
          var $trCloned = $flagForm.parents('tr.active.cloned');
          var $criteriaSelects = $flagForm.find('[id^="edit-criteria"] select');
          
          // Initialize whether entry has been scored by this user.
          // "Scored" = all criteria values submitted.
          if ($criteriaSelects.length && $trCloned.data('currentlyScored') === undefined) {
            var currentlyScored = true;
            $criteriaSelects.each(function() {
              if ($(this).val() === '') {
                currentlyScored = false;
              }
            });
            $trCloned.data('currentlyScored', currentlyScored);
          }
          
          var $submit = $flagForm.find('.form-submit.ajax-processed');
          if (Drupal.ajax[$submit.attr('id')] !== undefined) {
            var ajaxObj = Drupal.ajax[$submit.attr('id')];
            
            var originalBeforeSend = ajaxObj.options.beforeSend;
            ajaxObj.options.beforeSend = function(xmlhttprequest, options) {
              originalBeforeSend.call(this, xmlhttprequest, options);
              
              // TODO this is hacky
              var $failOpt = $criteriaSelects.find('option[value="0"]').eq(0);
              var isPassFail = ($failOpt.length && $failOpt.text() === 'Fail');
              
              // Score criteria.
              // "Scored" = all criteria values submitted.
              var scored = true;
              var passed = true;
              $criteriaSelects.each(function() {
                var val = $(this).val();
                if (val === '') {
                  scored = false;
                }
                
                // Pass/fail round: pass = 100%, so any 0s = fail.
                // Since missing scores result in 'fail' label currently, check
                // that also...
                // @see contest_entity_judging_form_contest_entity_admin_contest_entry_alter()
                // line ~2459
                if (isPassFail && (val === 0 || val === '0' || val === '')) {
                  passed = false;
                }
              });
              
              $trCloned.data('scored', scored);
              if (isPassFail) {
                $trCloned.data('passed', passed);
              }
              
              // Scores finalized checkbox
              $trCloned.data('finalized', $flagForm.find('.form-item-criteria-final input').prop('checked'));
              //console.log('setting data', $trCloned.data());
            }
          }
          $flagForm.addClass('submit-stash-process');
        }

    },
    confirmationToggle: function(context, settings) {

      // If successful confirmation message is visible, hide form entirely.
      $success = $('.alert.alert-success', context);
      if ($success.length) {
        $('form.confirmation', $success.parents('.judging-form'))
          .hide();

        // If set flag represents an exclusive queue, remove current row
        // from table.
        if (settings.contest_entity_judging.flag) {
          if (settings.contest_entity_judging.flag.is_queue_exclusive) {
            $success.parents('table')
              .find('> tbody > tr')
              .fadeTo('fast', 1, function() {
                var $self = $(this);
                if ($self.hasClass('active')) {
                  $self
                    .fadeOut(1400, function() {
                      $(this)
                        .remove();
                    });
                }
              });
          }
        }

        return;
      }

      // Force user to add a log message before setting this flag.
      $('form.confirmation', context).each(function(ix, e) {
        var $self = $(this);

        var $submit = $('input.disabled[type="submit"], button.disabled', $self);
        if (settings.contest_entity_judging.message.required) {
          $submit
            .fadeTo('fast', .50);
        }
        else {
          $submit
            .removeAttr('disabled')
            .removeClass('disabled');
        }

        $('a.cancel, a#edit-cancel', $self)
          .click(function(evt) {
            evt.preventDefault();

            $('.alert', $self.parents('.judging-form'))
              .hide();
            $self
              .fadeOut('fast', function() {
                //$(this).remove();
                // 2015-03-25 Tory: this was previously removed from DOM after
                // being hidden. Keep in place so toggle() above can access it
                // for front-end score status updates.

                // Show initial intro markup again.
                $('.judging-form-intro', context.parent())
                  .fadeIn('fast');
              });
          });

        $('textarea', $self)
          .keyup(function() {
            if (settings.contest_entity_judging.message.required) {
              var content = $(this).val().trim();
              if (content !== '') {
                $submit
                  .removeClass('disabled')
                  .fadeTo('fast', 1)
                  .removeAttr('disabled');
              }
              else {
                $submit
                  .fadeTo('fast', .50)
                  .addClass('disabled')
                  .attr('disabled', 'disabled');
              }
            }
          });
      });

    },
    /**
     * Set our own show/hide behavior on the fieldsets containing "Notes"
     * (judging history), etc. when an entry is expanded in the entries list.
     *
     * NOTE: core's misc/collapse.js is excluded, and replaced by Bootstrap
     * theme's own version, sites/all/themes/bootstrap/js/misc/_collapse.js.
     * That's intended to work with the bootstrap_panel theme hook which may
     * be collapsible. But these fieldsets are not themed as bootstrap panels.
     * Essentially we're gonna do our own thing here.
     */
    fieldsetToggle: function(context, settings) {
      var $context = $(context);

      // Only operate on stuff in rows of the entries list table.
      if ($context.parents('table.contest-entity-admin-contest-entry-results').length == 0) {
        return;
      }

      // Function to hide all fieldsets that are open in the currently-open
      // entry row.
      var fieldsetCollapse = function(evt) {
        evt.preventDefault();

        var $fieldsets = $(this)
          .parents('.active')
          .find('fieldset.collapsible')
          .addClass('collapsed')
          .removeClass('expanded');

        $('.fieldset-wrapper', $fieldsets)
          .hide();
      };

      var $fieldsetsCollap = $context.find('fieldset.collapsible');

      // Because core's collapse.js is not in play, make sure the
      // `a.fieldset-title` links are injected.
      // Adapted from collapse.js - Drupal.behaviors.collapse.attach()
      // (First remove the class indicating once('collapse') was run by
      // Bootstrap...)
      $fieldsetsCollap
        .removeClass('collapse-processed')
        .once('cej-collapse', function () {
          var $legend = $(this).find('legend');

          if (!$legend.find('a.fieldset-title').length) {
            // Link surrounds everything that is within <legend>.
            // Then <legend> contains link.
            var $link = $('<a class="fieldset-title" href="#"></a>');
            $link.append($legend.contents());
            $legend.append($link);
          }
      });

      // Bind fieldset toggle links.
      $('a.fieldset-title', $fieldsetsCollap)
        // Remove any core or Bootstrap behaviors.
        .unbind('click')
        .click(fieldsetCollapse)
        .click(function(evt) {
          evt.preventDefault();
          var $self = $(this);

          var $titles = $self
            .parents('.active')
            .find('a.fieldset-title');

          if ($self.hasClass('expanded')) {
            $titles
              .removeClass('expanded');

            return false;
          }
          else {
            $titles
              .removeClass('expanded');
            $self
              .addClass('expanded');
          }

          var $fieldset = $self
            .parents('fieldset.collapsible');
          $fieldset
            .removeClass('collapsed')
            .addClass('expanded');

          var $wrapper = $('.fieldset-wrapper', $fieldset);

          // Prepend a "close" link to fieldset wrapper.
          if (!$('a.close-fieldset', $wrapper).length) {
            $wrapper
              .prepend(
                $(document.createElement('a'))
                  .text(Drupal.t('close'))
                  .prop('href', '#')
                  .addClass('close-fieldset')
                );

            Drupal.attachBehaviors($fieldset, settings);
          }

          // Show fieldset wrapper.
          $wrapper
            .fadeIn('fast');
        });

      // Bind fieldset close link.
      $('.fieldset-wrapper a.close-fieldset', $context)
        .click(fieldsetCollapse)
        .click(function(evt) {
          var $self = $(this);
          $self
            .parents('.active')
            .find('a.fieldset-title')
            .removeClass('expanded');
        });
    },
    loadScoreDetails: function(context, settings) {
      var $links = $('table.contest-entity-admin-contest-entry-results a.judging-score-detail', context);
      if (!$links.length) {
        return;
      }

      // Bind AJAX links to retrieve scoring details.
      $links.click(function(evt) {
        evt.preventDefault();

        var $self = $(this);
        var $scores = $self.find('span.scores');

        if ($scores.is(':visible')) {
          $scores.hide();
        }
        else {
          var $label = $self.find('span.init');
          var $data = $self.find('span.data');

          $data = $.parseJSON($data.html());
          $self.attr('rel', $label.text());
          $label.text(' ' + Drupal.t('loading...'));

          // @see contest_entity_judging_get_contest_entry_score_details()
          $.ajax({
            url: '/contests/entry/' + $data.ceid + '/scores/' + $data.flag_name,
            data: '',
            success: function(data) {
              $links
                .find('span.scores')
                .hide();
              $label
                .text($self.attr('rel'));
              $scores
                .html(data)
                .fadeIn(200);
            },
            dataType: 'json'
          });
        }
      });
    },
    judgeAssignCountLimit: function(context, settings) {

      var selected = $(".judging-form input[type='checkbox']:checked").length
      var limit = $(".judging-form input[name='criteria_count']").val();
      if (selected >= limit) {
        $(".judging-form input[type='checkbox']").each(function(index){
          if (!$(this).is(":checked")) {
            $(this).attr('disabled', 'disabled');
            $(this).addClass('disabled');
          }
        });
      }

      $(".judging-form input[type='checkbox']").change(function(e){
        var selected = $(".judging-form input[type='checkbox']:checked").length
        var limit = $(".judging-form input[name='criteria_count']").val();
        if (selected >= limit) {
          $(".judging-form input[type='checkbox']").each(function(index){
            if (!$(this).is(":checked")) {
              $(this).attr('disabled', 'disabled');
              $(this).addClass('disabled');
            }
          });
        } else {
          $(".judging-form input[type='checkbox']").each(function(index){
            if (!$(this).is(":checked")) {
              $(this).removeAttr('disabled', 'disabled');
              $(this).removeClass('disabled');
            }
          });
        }
      });

    },
    labelEmptyEntryForms: function(context, settings) {

      // If an entry form renders blank, insert a message
      // @see contest_entity_judging_entity_view_alter()
      $('table.contest-entity-admin-contest-entry-results div.entry-fieldsets fieldset').each(function(index){
        var $el = $(this).find('div.entity div.content');
        if ($.trim($el.html()) == '') {
          $(this).find('.entity').addClass('entity-blank');
          $(this).find('.entity .content').html('Data is not yet available');
        }
      });

    }
  };

})(jQuery);
