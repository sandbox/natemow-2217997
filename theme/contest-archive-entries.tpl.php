<?php
/**
 * @file
 * contest-archive-entries.tpl.php
 * 
 * @see contest_entity_block_view_archive_contest_type_form_changed(),
 *   template_preprocess_contest_archive_entries()
 * 
 * Variables:
 *   $entries - array of entries rendered in teaser view mode
 */
?>
<ul class="<?php print $classes; ?>">
  <?php foreach ($entries as $entry): ?>
    <li><?php print render($entry); ?></li>
  <?php endforeach; ?>
</ul>
