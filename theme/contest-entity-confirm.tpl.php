<?php
/**
 * @file
 * contest-entity-confirm.tpl.php
 */
?>

<?php print render($message); ?>

<?php if ($contest_entry->social_url) : ?>
<p>
  Here's your unique URL to use on social networks:<br />
  <strong><?php print $contest_entry->social_url; ?></strong>
</p>
<?php endif; ?>
