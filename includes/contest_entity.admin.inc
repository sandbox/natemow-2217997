<?php
/**
 * @file
 * contest_entity.admin.inc
 */

// Main entry administration callback.
/**
 * Implements hook_query_CONTEST_ENTITY_ADMIN_CONTEST_ENTRY_RESULTS_sort().
 */
function contest_entity_query_CONTEST_ENTITY_ADMIN_CONTEST_ENTRY_RESULTS_sort_alter(&$query, &$header) {
  // Override $query ORDER BY if flag sort match is present.
  if ($query->hasTag('tablesort') && !empty($header)) {
    $params = drupal_get_query_parameters();
    $order_by =& $query->getOrderBy();
    if (!empty($params['order']) && !empty($params['sort'])) {
      foreach ($header as $key => $meta) {
        if ($params['order'] == $meta['data']) {
          $order_by = array($meta['field'] => $params['sort']);
        }
      }
    }
  }
}

function contest_entity_admin_contest_entry($form, &$form_state) {

  global $theme_key;

  drupal_add_css(drupal_get_path('module', 'contest_entity') . '/theme/contest_entity.css');

  // Make sure session contest is always populated.
  if (empty($_SESSION['contest_entry_filter']['contest'])) {
    $first = db_select('contest', 'c')
      ->fields('c', array('cid'))
      ->orderBy('weight')
      ->range(0, 1)
      ->execute()
      ->fetchField();

    $_SESSION['contest_entry_filter']['contest'] = $first;
  }

  $operations = module_invoke_all('contest_entry_operations', $_SESSION['contest_entry_filter']);

  // Require confirmation before proceeding with batch operation.
  if (isset($form_state['values']['operation']) && (!empty($form_state['values']['op']) && $form_state['values']['op'] == t('Continue'))) {
    $operation = $operations[$form_state['values']['operation']];
    if (!empty($form_state['values']['confirm_required'])) {
      return contest_entity_admin_contest_entry_batch_confirm($form, $form_state);
    }
  }

  drupal_set_title(t('Entries'));

  // Build the filters form.
  $form['#attributes']['class'][] = 'contest-entity-admin-contest-entry';
  $form['#submit'][] = 'contest_entity_admin_contest_entry_filter_submit';

  $form['filters'] = array(
    '#access' => user_access('administer contests'),
    '#type' => 'fieldset',
    '#title' => t('Filters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    // 2015-03-26 Tory: removed #theme 'exposed_filters__node' because
    // override bootstrap_exposed_filters() was looking for a form element only
    // present in the admin content list filters (I think) - causing error in
    // element_children().
    // Instead we add Bootstrap's class 'form-horizontal' here to fieldset to
    // align labels next to their inputs.
    '#attributes' => array(
      'class' => array('filters', 'form-horizontal'),
    ),
  );

  // Get filters defined by modules.
  $filters = module_invoke_all('contest_entry_filters', $_SESSION['contest_entry_filter']);
  $filters_session = array();
  if (!empty($_SESSION['contest_entry_filter'])) {
    // Prepopulate filters with session data.
    $filters_session = &$_SESSION['contest_entry_filter'];
    foreach ($filters_session as $key => $value) {
      $filters[$key]['#default_value'] = $value;
    }
  }
  else {
    // Set a default contest so initial entry list matches up.
    $default_value = $filters['contest']['#options'];
    $default_value = array_keys($default_value);
    if (!empty($default_value)) {
      $filters['contest']['#default_value'] = $default_value[0];
    }
  }

  // Add filter elements defined by modules.
  foreach ($filters as $key => &$meta) {
    $form['filters'][$key] = $meta;
    $form['filters'][$key]['#validated'] = TRUE;

    // Make title less Drupal-y.
    if ($theme_key == 'bootstrap' && !empty($form['filters'][$key]['#title'])) {
      $form['filters'][$key]['#title'] = ucwords($form['filters'][$key]['#title']);
    }
    // Default filter type.
    if (empty($form['filters'][$key]['#type'])) {
      $form['filters'][$key]['#type'] = 'select';
    }
    if (empty($form['filters'][$key]['#options'])) {
      $form['filters'][$key]['#options'] = array();
    }
    // Prepend [any] option.
    if (empty($form['filters'][$key]['#required']) && !empty($form['filters'][$key]['#options'])) {
      $form['filters'][$key]['#options'] = array('' => t('any')) + $form['filters'][$key]['#options'];
    }
    // Enable filters fieldset if user has access to this filter.
    if (!empty($meta['#access'])) {
      $form['filters']['#access'] = TRUE;
    }
    if (empty($form['filters'][$key]['#title'])) {
      unset($form['filters'][$key]);
    }
  }
  if (!user_access('administer contests')) {
    $form['filters']['contest']['#type'] = 'value';
    $form['filters']['contest']['#value'] = $filters['contest']['#default_value'];
  }

  // Add AJAX wrapper to hold contest-dependent fields.
  $form['filters']['contest_fields'] = array(
    '#weight' => -5,
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'contest-fields',
    ),
  );
  // Initial population of AJAX wrapper.
  $form_state['values']['contest'] = (!empty($filters['contest']['#default_value']) ? $filters['contest']['#default_value'] : NULL);
  if (empty($form_state['submitted'])) {
    contest_entity_admin_contest_entry_filter_changed($form, $form_state);
  }
  // Add filter buttons.
  $form['filters']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#attributes' => array('class' => array('btn-primary')),
  );


  // EFQ and apply $filter_session via entityCondition, propertyCondition and
  // fieldCondition methods; way easier than trying to JOIN via db_select
  // below. And we can store non-paged results for later use.
  $query_base = new EntityFieldQuery();
  $query_base = $query_base
    ->entityCondition('entity_type', 'contest_entry')
    ->entityCondition('bundle', $form_state['values']['contest']);
  // Alter $query_base per $filters_session applied. Disallow alter
  // of $filters_session pairs. Force alter to happen even if $_SESSION is not
  // yet set.
  $filters_context = (!empty($filters_session) ? $filters_session : array('contest' => $filters['contest']['#default_value']));
  drupal_alter('contest_entry_filters_query', $query_base, $filters_context);

  $results = $query_base->execute();
  $results = (!empty($results) ? array_keys($results['contest_entry']) : array());

  // Store EFQ results in $form_state for later use.
  $form_state['filter_results'] = $results;
  $form['filter_results'] = array(
    '#type' => 'value',
    '#value' => $results,
  );

  // Great job, now output table of all contest entries.
  $header = array(
    'user' => array('data' => t('ID'), 'field' => 'ce.ceid'),
    'remote_addr' => array('data' => t('')),
    'changed' => array('data' => t('Updated'), 'field' => 'ce.changed', 'sort' => 'desc'),
    'links' => t('Operations'),
  );

  $show_table_user_data = (user_access('administer contests') || user_access('administer users') || user_access('access user profiles'));
  if ($show_table_user_data) {
    $header = array_merge($header, array(
      'user' => array('data' => t('Username'), 'field' => 'u.name'),
      'remote_addr' => array('data' => t('IP Address'), 'field' => 'ce.remote_addr'),
    ));
  }

  $query = db_select('contest_entry', 'ce')
    ->fields('ce', array('ceid', 'remote_addr'))
    ->extend('TableSort')
    ->orderByHeader($header);

  // Apply results of EFQ above.
  if (!empty($results)) {
    $query->condition('ce.ceid', $results, 'IN');
  }
  else {
    $query->condition('ce.ceid', 0);
  }

  // Join contest data.
  $query->join('contest', 'c', 'c.cid = ce.cid');
  $query->fields('c', array('title'));
  // Join user data.
  $query->join('users', 'u', 'u.uid = ce.uid');
  $query->fields('u', array('uid', 'name'));
  // Add tag and metadata so modules can alter UI results as needed.
  $query->addTag('CONTEST_ENTITY_ADMIN_CONTEST_ENTRY_RESULTS');
  $query->addMetaData('filters', $filters_session);

  // Do paging stuff.
  $page_size = 50;
  if (!empty($filters_session['page_size'])) {
    $page_size = (is_numeric($filters_session['page_size']) ? $filters_session['page_size'] : NULL);
  }
  if (!empty($page_size)) {
    $query = $query->extend('PagerDefault');
    $query->limit($page_size);
  }

  $results = $query
    ->execute()
    ->fetchAllAssoc('ceid');

  $entries = contest_entry_load_multiple(array_keys($results));
  $path_alias = drupal_get_path_alias();
  $options = $options_data = array();
  foreach ($entries as $entity_id => &$entity) {
    $entity_label = entity_label('contest_entry', $entity);
    $entity_uri = entity_uri('contest_entry', $entity);
    $entity_uri = $entity_uri['path'];

    $username = $results[$entity_id]->name;
    $remote_addr = '';
    $contest_label = '';
    if (empty($results[$entity_id]->uid)) {
      $username = t('anonymous user');
    }
    if ($show_table_user_data) {
      $username = l($username, 'user/' . $results[$entity_id]->uid);
      $remote_addr = $entity->remote_addr;
    }
    else {
      $username = $entity_id;
    }

    // Set row operations links.
    $links = array();
    if (user_access('view contest ' . $form_state['values']['contest'] . ' entry')) {
      // Note: when judging is active, this link is altered to AJAX-request
      // the rendered entry and append it to the table row.
      // @see Drupal.behaviors.contest_entity_judging.rowToggle(),
      //   contest-entry.tpl.php (in site's front-end theme)
      $links['view'] = array(
        'title' => 'view',
        'href' => $entity_uri,
        'query' => array(
          'destination' => $path_alias,
        ),
      );
    }
    if (user_access('administer contests')) {
      $links['edit'] = array(
        'title' => 'edit',
        'href' => $entity_uri . '/edit',
        'query' => array(
          'destination' => $path_alias,
        ),
      );
      $links['delete'] = array(
        'title' => 'delete',
        'href' => $entity_uri . '/delete',
        'query' => array(
          'destination' => $path_alias,
        ),
      );
    }

    $options_data[$entity_id] = $results[$entity_id];
    $options[$entity_id] = array(
      // TODO: #data is only for legacy support. Now preferable to access via
      // $form['contest_entry']['#data'][$entity_id].
      '#data' => array(
        'contest_entry' => $entity,
        'row' => $results[$entity_id],
      ),
      'user' => $username,
      'remote_addr' => $remote_addr,
      'changed' => format_date($entity->changed, 'short'),
      'links' => theme_links(array(
        'links' => $links,
        'attributes' => array('class' => array('links', 'inline')),
        'heading' => NULL,
      )),
    );

    if (!$show_table_user_data) {
      unset($options[$entity_id]['remote_addr']);
    }
  }

  if (!$show_table_user_data) {
    unset($header['remote_addr']);
  }


  // Get the full contest entity.
  $contest = contest_load($form_state['values']['contest']);

  // Invoke reports hook and allow modules to spit out whatever they want.
  $filters_reports_result = module_invoke_all('contest_entry_filters_reports', $form_state['filter_results'], $contest, $filters_session);
  $filters_reports = array();
  foreach ($filters_reports_result as &$filters_report) {
    $filters_reports[] = array(
      'data' => array(
        $filters_report['label'],
        $filters_report['data'],
      ),
    );
  }

  // Output filtered reports fieldset.
  $form['filters']['#title'] = t('Filters: @count filtered entries', array(
    '@count' => count($form_state['filter_results']),
  ));
  $form['reports'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reports'),
    '#description' => '<p>'
      . t('Reports are based on the !filters and !settings for %contest.', array(
        '!filters' => l(t('active filters'), $path_alias, array(
          'fragment' => 'edit-filters',
        )),
        '!settings' => (!user_access('administer contests') ? t('settings') : l(t('settings'), Contest::defaultUriAdmin() . '/manage/' . entity_id('contest', $contest), array(
          'query' => array(
            'destination' => $path_alias,
          ),
        ))),
        '%contest' => entity_label('contest', $contest),
      ))
    . '</p>',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => (user_access('administer contests') && !empty($options)),
  );

  $form['reports']['table'] = array(
    '#markup' => theme('table', array(
      'rows' => $filters_reports,
    )),
  );

  // Output the bulk operations fieldset.
  $options_operation = array();
  foreach ($operations as $key => $meta) {
    $allow = TRUE;
    if (in_array($key, array(
      'delete_filtered',
      'delete_selected',
    ))) {
      $allow = !empty($contest->bulk_ops_delete);
    }

    if ($allow) {
      $options_operation[$key] = $meta['label'];
    }
  }

  $form['bulk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk operations'),
    '#description' => '<p>' . t('Filter-based operations are not dependent on record selection or page size. Operations will occur in batches of @batch_size.', array(
      '@batch_size' => CONTEST_ENTITY_BATCH_SIZE,
    )) . '</p>',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => (count($options_operation) > 0),
  );
  $form['bulk']['confirm_required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require confirmation?'),
    '#description' => t('The PHP %max_input_vars variable is currently allowing @max_input_vars input values per form. Skipping confirmation is recommended for operations affecting more than @max_input_vars records. Note that the same limitation of @max_input_vars is also relevant to the currently filtered page size@page_size.', array(
      '%max_input_vars' => 'max_input_vars',
      '@max_input_vars' => ini_get('max_input_vars'),
      '@page_size' => (!empty($page_size) ? " ({$page_size} records)" : ""),
    )),
    '#default_value' => TRUE,
  );
  $form['bulk']['inline'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['bulk']['inline']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options_operation,
  );
  $form['bulk']['inline']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#validate' => array('contest_entity_admin_contest_entry_batch_validate'),
    '#submit' => array('contest_entity_admin_contest_entry_batch_submit'),
  );

  // Output results table.
  $form['contest_entry'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No entries queued.'),
    '#attributes' => array(
      // "tableheader-processed" forces Drupal to make this table non-sticky.
      'class' => array('contest-entity-admin-contest-entry-results', 'tableheader-processed'),
    ),
    '#data' => $options_data,
  );

  // Hide fieldsets and just output an empty message.
  if (empty($options)) {
    $form['reports']['#access'] = FALSE;
    $form['bulk']['#access'] = FALSE;
    $form['contest_entry']['#type'] = 'markup';
    $form['contest_entry']['#markup'] = $form['contest_entry']['#empty'];
  }

  // Output results pager.
  pager_default_initialize(count($form_state['filter_results']), $page_size);
  $form['pager'] = array(
    '#theme' => 'pager',
  );

  drupal_add_library('system', 'jquery.form');
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_library('system', 'drupal.collapse');
  drupal_add_library('system', 'drupal.progress');

  return $form;
}

function contest_entity_admin_contest_entry_filter_submit($form, &$form_state) {

  if (!empty($form_state['values']['op']) && $form_state['values']['op'] == t('Filter')) {
    $filters = module_invoke_all('contest_entry_filters', $_SESSION['contest_entry_filter']);
    $_SESSION['contest_entry_filter'] = array();

    foreach ($filters as $key => $meta) {
      // Check if a select has a valid option of 0 and if such was submitted.
      $zero_val = !empty($meta['#options']) && !empty($meta['#options'][0])
        && isset($form_state['values'][$key])
        && ($form_state['values'][$key] === 0 || $form_state['values'][$key] === '0');
      
      if ((!empty($form_state['values'][$key]) || $zero_val) && $form_state['values'][$key] != '[any]') {
        $_SESSION['contest_entry_filter'][$key] = $form_state['values'][$key];
      }
      else {
        unset($_SESSION['contest_entry_filter'][$key]);
      }
    }
  }

}

function contest_entity_admin_contest_entry_filter_changed(&$form, &$form_state) {
  
  // Make sure this file is loaded during AJAX requests.
  form_load_include($form_state, 'inc', 'contest_entity', 'includes/contest_entity.admin');

  // Move role filter to AJAX container, since its options need to be
  // populated onchange.
  if (empty($form['filters']['contest_fields']['role'])) {
    $form['filters']['contest_fields']['role'] = $form['filters']['role'];
    unset($form['filters']['role']);
  }

  // Set role options per defined permissions.
  $options_role = array();
  if (!$form['filters']['contest_fields']['role']['#required']) {
    $options_role = array('' => t('any'));
  }
  $options_role += user_roles(FALSE, 'create contest ' . $form_state['values']['contest'] . ' entry');
  $form['filters']['contest_fields']['role']['#options'] = $options_role;

  return $form['filters']['contest_fields'];


  // TODO: Fix attachment of fields per contest input.
  $form['filters']['contest_fields']['fields'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('contest fields'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array(
      // Manually add classes so Drupal reattaches behaviors on AJAX update.
      'class' => array('collapsible', 'collapsed'),
      'style' => 'margin-left: 10em;',
    ),
  );

  // Attach additional field filters per contest bundle.
  $entity = entity_create('contest_entry', array('cid' => $form_state['values']['contest']));
  field_attach_form('contest_entry', $entity, $form['filters']['contest_fields']['fields'], $form_state);

  return $form['filters']['contest_fields'];
}

// Batch ops.
function contest_entity_admin_contest_entry_batch_validate($form, &$form_state) {

  $operations = module_invoke_all('contest_entry_operations');
  $operation = $operations[$form_state['values']['operation']];

  // Validate for selected rows if specified by operation.
  if ($operation['validate selected']) {
    if (!is_array($form_state['values']['contest_entry']) || !count(array_filter($form_state['values']['contest_entry']))) {
      form_set_error('', t('No items selected.'));
    }
  }
}

function contest_entity_admin_contest_entry_batch_confirm(&$form, &$form_state) {

  $operations = module_invoke_all('contest_entry_operations');
  $operation = $operations[$form_state['values']['operation']];

  $form = confirm_form(
    $form,
    t('Are you sure you want to %operation?', array('%operation' => $operation['label'])),
    drupal_get_path_alias(),
    t('This action cannot be undone and may take several minutes to run.'),
    t('Confirm'),
    t('Cancel')
  );

  $form['actions']['submit']['#submit'] = array('contest_entity_admin_contest_entry_batch_submit');


  // Default to filtered records.
  $entries = $form_state['filter_results'];
  // Use selected if specified.
  if ($operation['validate selected']) {
    $entries = array_filter($form_state['values']['contest_entry']);
  }

  $form['operation'] = array(
    '#type' => 'hidden',
    '#value' => $form_state['values']['operation'],
  );

  $form['contest_entry'] = array(
    '#prefix' => '<div>',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  // TODO: Improve performance here; Drupal will collapse on 40K hidden inputs
  // being passed. One option is potentially just writing out inputs to string
  // but then PHP's max_input_vars is still in play.
  //
  //$contest_entry = '';
  foreach ($entries as $entry) {
    //$contest_entry .= vsprintf('<input type="hidden" name="contest_entry[%d]" value="%d">', array($entry, $entry));
    $form['contest_entry'][$entry] = array(
      '#type' => 'hidden',
      '#value' => $entry,
    );
  }

//  $form['contest_entry'] = array(
//    '#markup' => $contest_entry,
//  );

  $form['description']['#markup'] = t('@count contest @entry will be affected.', array(
    '@count' => count($entries),
    '@entry' => format_plural(count($entries), 'entry', 'entries'),
  )) . ' ' . $form['description']['#markup'];

  return $form;
}

function contest_entity_admin_contest_entry_batch_submit($form, &$form_state) {

  $operations = module_invoke_all('contest_entry_operations');
  $operation = $operations[$form_state['values']['operation']];

  // Require confirmation before proceeding with batch operation.
  if (!empty($form_state['values']['confirm_required'])) {
    $form_state['rebuild'] = TRUE;
    return;
  }

  if (!empty($operation['batch'])) {
    // Set default batch args.
    $batch_defaults = array(
      'title' => t('Contest Entry batch operation: %title', array(
        '%title' => $operation['label'],
      )),
      'file' => drupal_get_path('module', 'contest_entity') . '/includes/contest_entity.admin.inc',
      'finished' => 'contest_entity_admin_contest_entry_batch_finish',
      'load entities' => TRUE,
    );

    $operation['batch'] = array_merge($batch_defaults, $operation['batch']);
    
    // TORY unfinished test
//    if (!empty($operation['batch']['custom_query'])) {
//      $args = array();
//      if (!empty($operation['batch']['custom_query_args'])) {
//        $args = $operation['batch']['custom_query_args'];
//      }
//      $rows = db_query($operation['batch']['custom_query'], $args)->fetchAll();
//      // naming...
//      $entries = $rows;
//    }
//    else {

      // Default to filtered records.
      $entries = &$form_state['filter_results'];
      // Use selected if specified.
      if ($operation['validate selected']) {
        $entries = array_filter($form_state['values']['contest_entry']);
      }
//    }    

    // Start batch ops.
    contest_entity_admin_contest_entry_batch_start($operation['batch'], $entries);
    cache_clear_all();
  }
}

function contest_entity_admin_contest_entry_batch_start($batch_args, &$entries) {
  $operations = array();
  if (!empty($entries)) {
    if (!empty($batch_args['first'])) {
      // Initial batch op.
      $first = array($entries[0]);
      $operations[] = array($batch_args['first'], array($first, 0));
    }

    if (!empty($batch_args['chunk'])) {
      // Batch in chunks of n.
      $batch_chunk = array();
      $batch_total = 0;
      foreach ($entries as &$entry) {
        $batch_chunk[] = $entry;

        if (count($batch_chunk) == CONTEST_ENTITY_BATCH_SIZE) {
          // chunk size reached. Store and start a new chunk
          $operations[] = array($batch_args['chunk'], array($batch_chunk, ($batch_total + 1)));
          $batch_chunk = array();
        }

        $batch_total++;
      }
      // Queue the delta of $batch_chunk to a final operation.
      if (!empty($batch_chunk)) {
        $operations[] = array($batch_args['chunk'], array($batch_chunk, $batch_total));
      }
    }
  }

  // Start the batch.
  if (!empty($operations)) {
    $batch = array_merge(
      array(
        'operations' => $operations,
      ),
      $batch_args
    );

    batch_set($batch);
  }
}

function contest_entity_admin_contest_entry_batch_finish($success, $results, $operations) {
  // :(
  if (isset($_SESSION['contest_entity_batch_store'])) {
    unset($_SESSION['contest_entity_batch_store']);
  }
  
  if (!$success) {
    drupal_set_message(t('Finished with an error.'), 'error');
  }

  foreach ($results as $message) {
    drupal_set_message($message);
  }
}

// Export ops.
function _contest_entity_admin_contest_entry_batch_op_export_filtered_field(&$meta, $entity_type, $is_label = TRUE, &$entity_metadata_wrapper_value = NULL) {
  // Preset "complex" field types.
  $fields_complex = &drupal_static(__FUNCTION__ . '_fields_complex');
  if (empty($fields_complex)) {
    $fields_complex = array(
      'field_item_link',
      'field_item_location',
    );
  }

  // Preset entityreference field types.
  $fields_entityreference = &drupal_static(__FUNCTION__ . '_fields_entity');
  if (empty($fields_entityreference)) {
    // Already handled by other means -- this is for special handling.
    $ignored = array(
      'comment',
      'contest_type',
      'contest',
      'contest_entry',
      'node',
      'file',
      'taxonomy_term',
      'taxonomy_vocabulary',
      'user',
    );

    $fields_entityreference = array();
    $bundles = entity_get_info();
    foreach ($bundles as $key => $bundle) {
      if (!in_array($key, $ignored)) {
        if ($bundles == FALSE) {
          $fields_entityreference[] = $key;
        }
        else {
          $fields_entityreference[$key] = $bundle;
        }
      }
    }
  }

  // Preset timestamp field names.
  $fields_timestamp = &drupal_static(__FUNCTION__ . '_fields_timestamp');
  if (empty($fields_timestamp)) {
    $fields_timestamp = array(
      'created',
      'changed',
      'access',
      'login',
    );
  }

  // Support for location_cck fields.
  $location_entity_key_map = &drupal_static(__FUNCTION__ . '_location_entity');
  if (empty($location_entity_key_map)) {
    $location_entity_key_map = (module_exists('location_entity') ? location_entity_key_map() : array());
  }

  $output = array();
  // Extract complex field labels or values.
  if (!empty($meta['type']) && array_key_exists($meta['type'], $fields_entityreference)) {
    // $output[] = 'CAUGHT THIS ONE: '.$meta['type'];
  }
  else if (!empty($meta['type']) && in_array($meta['type'], $fields_complex)) {
    switch ($meta['type']) {
      // Handle link fields.
      case 'field_item_link':
        $output[] = ($is_label ? $meta['label'] : theme('link_formatter_link_plain', array('element' => $entity_metadata_wrapper_value)));
        break;

      // Handle location_cck fields.
      case 'field_item_location':
        if (!empty($location_entity_key_map)) {
          foreach ($location_entity_key_map as $lkey => &$lmeta) {
            $output[] = ( $is_label ? $lmeta['label'] : location_entity_field_verbatim_get( $entity_metadata_wrapper_value, array(), $lkey, $entity_type ) );
          }

          break;
        }
      case 'entityreference':
        if ( is_array($entity_metadata_wrapper_value) ) {
          // Multiple values
          foreach ($entity_metadata_wrapper_value as $instance) {

          }
        } else {
          // Single value - object
        }
        break;
      // Fail over to default handling.
      default:
        $output[] = ($is_label ? $meta['label'] : $entity_metadata_wrapper_value);
        break;
    }
  }
  // Extract timestamp field labels or values.
  elseif (!empty($meta['schema field']) && in_array($meta['schema field'], $fields_timestamp)) {
    switch ($meta['schema field']) {
      // Handle timestamp fields.
      case 'created':
      case 'changed':
        $output[] = ($is_label ? $meta['label'] : (!empty($entity_metadata_wrapper_value) ? format_date($entity_metadata_wrapper_value, 'short') : ''));
        break;

      case 'access':
      case 'login':
        if ($entity_type == 'user') {
          $output[] = ($is_label ? $meta['label'] : (!empty($entity_metadata_wrapper_value) ? format_date($entity_metadata_wrapper_value, 'short') : ''));
          break;
        }

      // Fail over to default handling.
      default:
        $output[] = ($is_label ? $meta['label'] : $entity_metadata_wrapper_value);
        break;
    }
  }
  // Extract generic other array-based field labels or values.
  elseif (is_array($entity_metadata_wrapper_value)) {
    $output[] = ($is_label ? $meta['label'] : print_r($entity_metadata_wrapper_value, TRUE));
  }
  // Extract simple field labels or values.
  else {
    $output[] = ($is_label ? $meta['label'] : $entity_metadata_wrapper_value);
  }

  // Strip out any HTML that might have been injected in to labels.
  if ($is_label) {
    foreach ($output as &$label) {
      $label = strip_tags($label);
    }
  }

  // Allow modules to alter the export output.
  $alter_context = array(
    'meta' => &$meta,
    'entity_type' => $entity_type,
    'is_label' => $is_label,
    'entity_metadata_wrapper_value' => &$entity_metadata_wrapper_value,
  );

  drupal_alter('contest_entry_field_export', $output, $alter_context);

  return $output;
}

function contest_entity_admin_contest_entry_batch_op_export_filtered_finish($success, $results, $operations) {

  $success = TRUE;
  $results = array(t('Exported @count contest entries. Your report can be downloaded here: !path', array(
    '@count' => $results[1],
    '!path' => l(basename($results[0]), $results[0]),
  )));
  contest_entity_admin_contest_entry_batch_finish($success, $results, $operations);
}

function contest_entity_admin_contest_entry_batch_op_export_filtered_labels($entries, $count, &$context) {

  $file_path = variable_get('file_public_path', conf_path() . '/files') . '/contest_entity';

  if (file_prepare_directory($file_path, FILE_CREATE_DIRECTORY)) {
    // Set file pointer.
    $file_name = 'export-' . REQUEST_TIME . '.csv';
    $context['results'] = array($file_path . '/' . $file_name);
    $handle = fopen($context['results'][0], 'a+');
    // Get exclusive file pointer lock.
    if (flock($handle, LOCK_EX)) {
      $columns = array();

      // Get property and field info for contest_entry entity.
      $entity_properties = entity_get_property_info('contest_entry');
      $entry = contest_entry_load($entries[0]);
      $_entry = $entry;
      $entry = entity_metadata_wrapper('contest_entry', $entry);
      foreach ($entity_properties['properties'] as $key => &$meta) {
        $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'contest_entry');
        $columns = array_merge($columns, $output);
      }
      $bundle_entry = $entry->cid->value();
      foreach ($entity_properties['bundles'][$bundle_entry]['properties'] as $key => &$meta) {
        $meta['bundle'] = $bundle_entry;
        if (isset($meta['field']) && $meta['field'] === TRUE) {
          $meta['field name'] = $key;
        }
        $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'contest_entry');
        $columns = array_merge($columns, $output);
      }

      // Get property and field info for user entity.
      $user_properties = entity_get_property_info('user');
      foreach ($user_properties['properties'] as $key => &$meta) {
        $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'user');
        $columns = array_merge($columns, $output);
      }
      $bundle_user = 'user';
      if (!empty($user_properties['bundles'][$bundle_user]['properties'])) {
        foreach ($user_properties['bundles']['user']['properties'] as $key => &$meta) {
          $meta['bundle'] = $bundle_user;
          if (isset($meta['field']) && $meta['field'] === TRUE) {
            $meta['field name'] = $key;
          }
          $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'user');
          $columns = array_merge($columns, $output);
        }
      }

      // Compile desired data for hook context.
      $row_context = array(
        'handle' => &$handle,
        'contest_entry' => &$_entry,                     // Loaded first entry
        'entry_meta' => &$entry,                         // Entry metadata wrapper
        'is_label' => TRUE,                             // Is this for the label row?
        'filters' => &$_SESSION['contest_entry_filter'],  // Active filters
      );

      // Invoke hook to allow modules to alter $columns.
      drupal_alter('contest_entry_batch_op_export_filtered', $columns, $row_context);

      // Write row.
      if (!empty($columns)) {
        @fputcsv($handle, $columns);
      }

      // Release file pointer lock.
      flock($handle, LOCK_UN);
    }
    // Close file pointer.
    fclose($handle);
  }
  else {
    $context['results'] = array();
  }

}

function contest_entity_admin_contest_entry_batch_op_export_filtered($entries, $count, &$context) {
  ini_set('memory_limit', -1);
  $output = '';
  // Get property and field info for user entity.
  $user_properties = entity_get_property_info('user');
  // Get property and field info for contest_entry entity.
  $entity_properties = entity_get_property_info('contest_entry');

  if (!empty($context['results'])) {
    $handle = fopen($context['results'][0], 'a');
    if (flock($handle, LOCK_EX)) {
      // Write rows for this batch group.
      $entries = contest_entry_load_multiple($entries);
      foreach ($entries as &$entry) {
        // Get contest_entry columns.
        $_entry = $entry;
        $entry = entity_metadata_wrapper('contest_entry', $entry);
        $columns = array();
        foreach ($entity_properties['properties'] as $key => &$meta) {
          $output = @$entry->{$key}->value();
          $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'contest_entry', FALSE, $output);
          $columns = array_merge($columns, $output);
        }
        foreach ($entity_properties['bundles'][$entry->cid->value()]['properties'] as $key => &$meta) {
          $output = @$entry->{$key}->value();
          $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'contest_entry', FALSE, $output);
          $columns = array_merge($columns, $output);
        }

        // Get user columns.
        $account = user_load($entry->uid->value());
        if (!empty($account)) {
          $account = entity_metadata_wrapper('user', $account);
          foreach ($user_properties['properties'] as $key => &$meta) {
            $output = @$account->{$key}->value();
            $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'user', FALSE, $output);
            $columns = array_merge($columns, $output);
          }
          if (!empty($user_properties['bundles']['user']['properties'])) {
            foreach ($user_properties['bundles']['user']['properties'] as $key => &$meta) {
              $output = @$account->{$key}->value();
              $output = _contest_entity_admin_contest_entry_batch_op_export_filtered_field($meta, 'user', FALSE, $output);
              $columns = array_merge($columns, $output);
            }
          }
        }

        // Compile desired data for hook context.
        $row_context = array(
          'handle' => &$handle,
          'contest_entry' => &$_entry,                     // Loaded first entry
          'entry_meta' => &$entry,                         // Entry metadata wrapper
          'is_label' => FALSE,                             // Is this for the label row?
          'filters' => &$_SESSION['contest_entry_filter'],  // Active filters
        );

        // Invoke hook to allow modules to alter $columns.
        drupal_alter('contest_entry_batch_op_export_filtered', $columns, $row_context);

        // Write row.
        if (!empty($columns)) {
          @fputcsv($handle, $columns);
        }
      }
      // Release file pointer lock.
      flock($handle, LOCK_UN);
    }

    // Close file pointer.
    fclose($handle);

    $context['results'][1] = $count;

    // Display progressbar message.
    $context['message'] = t('Exported @count contest entries.', array('@count' => $count));
  }
}

// Delete ops.
function contest_entity_admin_contest_entry_batch_op_delete($entries, $count, &$context) {

  entity_delete_multiple('contest_entry', $entries);

  // Display progressbar message.
  $context['message'] = t('Deleted @count contest entries.', array('@count' => $count));

  // Add 'finished' callback message.
  $context['results'] = array(t('Deleted @count contest entries.', array('@count' => $count)));
}

// Add contest_entry page callback.
function contest_entity_admin_contest_entry_add() {

  $content = array();

  $contests = new EntityFieldQuery();
  $contests = $contests
    ->entityCondition('entity_type', 'contest')
    ->propertyOrderBy('weight')
    ->execute();

  $contests = contest_load_multiple(array_keys($contests['contest']));

  foreach ($contests as $cid => $contest) {
    $contest_label = entity_label('contest', $contest);
    $contest_id = entity_id('contest', $contest);

    $content[$contest->cid] = array(
      'title' => $contest_label,
      'href' => 'contests/' . $contest_id,
      'localized_options' => array(),
      'description' => NULL,
    );
  }

  if (empty($content)) {
    return '<p>' . t('You have not created any contests yet. Go to the <a href="@create-contest">contest creation page</a> to add a new contest.', array('@create-contest' => url(Contest::defaultUriAdmin() . '/add'))) . '</p>';
  }
  else {
    return theme('node_add_list', array('content' => $content));
  }
}
