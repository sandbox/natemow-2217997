<?php
/**
 * @file
 * contest_entity.block.inc
 */

/**
 * Block view callback.
 */
function _contest_entity_block_view_archive_contest_type($args) {

  $block = array(
    'subject' => t('Archived Entries'),
    'content' => array(
      'filter' => drupal_get_form('contest_entity_block_view_archive_contest_type_form', $args['contest_type']),
    ),
  );

  return $block;
}

function contest_entity_block_view_archive_contest_type_form($form, &$form_state, $contest_type) {

  // Set initial contest selection.
  if (empty($form_state['values']['contest_type'])) {
    $form_state['values']['contest_type'] = $contest_type;
  }

  // Get contest per contest_type.
  $contests = new EntityFieldQuery();
  $contests = $contests
    ->entityCondition('entity_type', 'contest')
    ->entityCondition('bundle', entity_id('contest_type', $contest_type))
    ->propertyCondition('mode', CONTEST_ENTITY_MODE_ARCHIVE)
    ->propertyOrderBy('weight', 'ASC')
    ->addTag('CONTEST_ENTITY_BLOCK_ARCHIVED_CONTEST')
    ->execute();

  $options = array();
  if (!empty($contests)) {
    $contests = array_keys($contests['contest']);
    $contests = contest_load_multiple($contests);

    foreach ($contests as $contest) {
      // Set initial contest selection.
      if (empty($form_state['values']['contest'])) {
        $form_state['values']['contest'] = entity_id('contest', $contest);
      }

      // Add contest option.
      $options[entity_id('contest', $contest)] = entity_label('contest', $contest);
    }
  }

  if (empty($options)) {
    return NULL;
  }

  $form['contest'] = array(
    '#type' => 'select',
    '#title' => t('Contest'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => $form_state['values']['contest'],
    '#executes_submit_callback' => TRUE,
    '#ajax' => array(
      'file' => 'includes/contest_entity.block.inc',
      'callback' => 'contest_entity_block_view_archive_contest_type_form_changed',
      'wrapper' => 'contest-entry',
      'effect' => 'fade',
    ),
  );

  // Add AJAX wrapper with contest_entry data.
  $form['contest_entry'] = contest_entity_block_view_archive_contest_type_form_changed($form, $form_state);

  return $form;
}

function contest_entity_block_view_archive_contest_type_form_changed($form, &$form_state) {

  $form['contest_entry'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'contest-entry',
      'class' => array('form-item', 'clearfix'),
    ),
  );

  $contest = contest_load($form_state['values']['contest']);
  
  // Prepend list of entries with contest data.
  $form['contest_entry']['contest'] = contest_entity_view($contest, 'full', LANGUAGE_NONE, 'contest');
  
  $form['contest_entry']['list'] = array(
    '#markup' => t('No entries available.'),
  );

  $contest_entries = new EntityFieldQuery();
  $contest_entries = $contest_entries
    ->entityCondition('entity_type', 'contest_entry')
    ->entityCondition('bundle', entity_id('contest', $contest))
    ->propertyOrderBy('weight')
    ->propertyOrderBy('changed', 'DESC')
    ->range(0, 10)
    ->addTag('CONTEST_ENTITY_BLOCK_ARCHIVED_CONTEST_ENTRY')
    ->execute();

  if (!empty($contest_entries)) {
    $contest_entries = array_keys($contest_entries['contest_entry']);
    $contest_entries = contest_entry_load_multiple($contest_entries);
    $contest_entries = contest_entity_view_multiple($contest_entries, 'teaser', LANGUAGE_NONE, 'contest_entry');

    // Add list of entries to output.
    $form['contest_entry']['list'] = $contest_entries;
    $form['contest_entry']['list']['#theme'] = 'contest_archive_entries';
    
    // (TORY - moved this out of $form['contest_entry']['list'] so that new
    // theme hook doesn't have to handle it)
//    // Prepend list of entries with contest data.
//    $contest = contest_entity_view($contest, 'full', LANGUAGE_NONE, 'contest');
//    $form['contest_entry']['list']['#prefix'] = drupal_render($contest);
  }

  return $form['contest_entry'];
}
