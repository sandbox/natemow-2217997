<?php
/**
 * @file
 * contest_entity.devel_generate.inc
 */

/**
 * Developer tool to generate dummy contest entries.
 */
function contest_entity_generate_contest_entry_form() {

  $options_contest = db_select('contest', 'c')
    ->fields('c', array('cid', 'title'))
    ->orderBy('weight')
    ->execute()
    ->fetchAllKeyed();

  $form['contests'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which contests should receive entries?'),
    '#options' => $options_contest,
    '#default_value' => array_keys($options_contest),
  );

  $form['num_contest_entries'] = array(
    '#type' => 'textfield',
    '#title' => t('How many entries would you like to generate per contest?'),
    '#default_value' => 50,
    '#size' => 10,
  );

  $form['kill_contest_entries'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete existing entries before generating new ones.'),
    '#default_value' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate'),
  );

  return $form;
}

function contest_entity_generate_contest_entry_form_submit($form, &$form_state) {
  contest_entity_generate_batch_content_entry_start($form_state);
}

function contest_entity_generate_batch_content_entry_start(&$form_state) {

  $form_state['values']['contests'] = array_keys(array_filter($form_state['values']['contests']));
  $contests = contest_load_multiple($form_state['values']['contests']);

  if (empty($contests)) {
    devel_generate_set_message(t('No contests selected.'), 'error');
    return;
  }

  $operations = array();

  // Add the kill operation.
  if ($form_state['values']['kill_contest_entries']) {
    $operations[] = array('contest_entity_generate_batch_content_entry_op_kill', array($contests));
  }

  // Add the operations to create the entities.
  if ($form_state['values']['num_contest_entries']) {
    foreach ($contests as $contest) {

      // Batch in chunks of n.
      $batch_chunk = array();
      $batch_total = 0;
      for ($i = 0; $i < $form_state['values']['num_contest_entries']; $i++) {
        $starting = ($i == 0);
        $finishing = (($i + 1) == $form_state['values']['num_contest_entries']);

        $entry = entity_create('contest_entry', array(
          'cid' => $contest->cid,
        ));

        $batch_chunk[] = $entry;

        if (count($batch_chunk) == CONTEST_ENTITY_BATCH_SIZE) {
          $operations[] = array('contest_entity_generate_batch_content_entry_op_add', array($batch_chunk, $starting, $finishing));
          $batch_chunk = array();
        }
      }
      // Queue the delta of $batch_chunk to a final operation.
      if (!empty($batch_chunk)) {
        $operations[] = array('contest_entity_generate_batch_content_entry_op_add', array($batch_chunk, $starting, $finishing));
      }

    }
  }

  // Start the batch.
  $batch = array(
    'file' => drupal_get_path('module', 'contest_entity') . '/includes/contest_entity.devel_generate.inc',
    'title' => t('Generating Contest Entries'),
    'operations' => $operations,
    'finished' => 'contest_entity_generate_batch_content_entry_finish',
  );

  batch_set($batch);
}

function contest_entity_generate_batch_content_entry_finish($success, $results, $operations) {
  if (!$success) {
    devel_generate_set_message(t('Finished with an error.'), 'error');
  }

  foreach ($results as $message) {
    devel_generate_set_message($message);
  }
}

function contest_entity_generate_batch_content_entry_op_kill($contests, &$context) {

  $ceid = new EntityFieldQuery();
  $ceid = $ceid
    ->entityCondition('entity_type', 'contest_entry')
    ->entityCondition('bundle', array_keys($contests), 'IN')
    ->execute();

  if (!empty($ceid)) {
    $contest_titles = array();
    foreach ($contests as $contest) {
      $contest_titles[] = entity_label('contest', $contest);
    }

    // Display progressbar message.
    $context['message'] = t('Deleting %contest contest entries.', array('%contest' => implode(', ', $contest_titles)));

    // Delete entities.
    $ceid = array_keys($ceid['contest_entry']);
    entity_delete_multiple('contest_entry', $ceid);

    // Add 'finished' callback message.
    $context['results'][] = t('Deleted @count %contest contest entries.', array('@count' => count($ceid), '%contest' => implode(', ', $contest_titles)));
  }
}

function contest_entity_generate_batch_content_entry_op_add($entries, $starting, $finishing, &$context) {

  module_load_include('inc', 'devel_generate', 'devel_generate');
  module_load_include('inc', 'devel_generate', 'devel_generate.fields');

  // Loop stub entities, populate fields and save.
  foreach ($entries as &$entry) {
    devel_generate_fields($entry, 'contest_entry', $entry->cid);
    if (entity_save('contest_entry', $entry)) {
      if (empty($context['results']['added_count'])) {
        $context['results']['added_count'] = 0;
      }

      $context['results']['added_count']++;
    }
  }

  // Display progressbar message.
  $context['message'] = t('Added @count contest entries.', array('@count' => $context['results']['added_count']));

  if ($finishing) {
    // Store some results for post-processing in the 'finished' callback.
    $context['results'][] = t('Added @count contest entries.', array('@count' => $context['results']['added_count']));
    unset($context['results']['added_count']);
  }

}
