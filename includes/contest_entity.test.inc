<?php
/**
 * @file
 * Utility functions to ease testing.
 * Currently none of these are called anywhere in the codebase. Might be good
 * to write actual step-through-scenario tests eventually.
 */

define('CE_TEST_FORMAT_DAY', 'Y-m-d');
define('CE_TEST_FORMAT_DAYTIME', 'Y-m-d H:i');


// Utilities


function ce_test_get_timezone_east_coast() {
  static $tz = NULL;
  if (empty($tz)) {
    $tz = new DateTimeZone('America/New_York');
  }
  
  return $tz;
}

function ce_test_get_timezone_west_coast() {
  static $tz = NULL;
  if (empty($tz)) {
    $tz = new DateTimeZone('America/Los_Angeles');
  }
  
  return $tz;
}

/**
 * Format a timestamp into a readable date/time string.
 * 
 * @param int $timestamp
 * @param string $coast - US timezone to use - 'east' or 'west'
 * @param int $format - one of the predefined formats,
 *   CE_TEST_FORMAT_DAY or CE_TEST_FORMAT_DAYTIME
 * 
 * @return string
 */
function ce_test_output_datetime($timestamp, $coast, $format) {
  $date = new DateTime('@' . $timestamp);
  if ($coast == 'east') {
    $date->setTimezone(ce_test_get_timezone_east_coast());
  }
  elseif ($coast == 'west') {
    $date->setTimezone(ce_test_get_timezone_west_coast());
  }
  return $date->format($format);
}


// Data output

function ce_test_list_all_entry_created_daytimes($cid) {
  $tz_east = ce_test_get_timezone_east_coast();
  $tz_west = ce_test_get_timezone_west_coast();

  $createds = db_query("SELECT ceid, created
    FROM contest_entry
    WHERE cid = :cid
    ORDER BY ceid ASC", array(
      ':cid' => $cid,
    ))->fetchAllKeyed();

  $log = array();
  
  foreach ($createds as $ceid => $created) {
    $d1 = new DateTime('@' . $created);
    $d1->setTimeZone($tz_east);
    $d2 = new DateTime('@' . $created);
    $d2->setTimeZone($tz_west);

    $log[] = $ceid . ': east = ' . $d1->format(CE_TEST_FORMAT_DAYTIME) . ' / west ' . $d2->format(CE_TEST_FORMAT_DAYTIME);
  }
  
  $log = implode("<br/>\n", $log);
  
  if (module_exists('devel')) {
    dsm($log);
  }
  return $log;
}


// Actions


function ce_test_set_entry_created_yesterday($ceid) {
  $entry = contest_entry_load($ceid);
  
  db_query("UPDATE contest_entry 
    SET created = :created 
    WHERE ceid = :ceid", array(
      ':created' => $entry->created - (60 * 60 * 24),
      ':ceid' => $ceid,
    ));
  
  $check = db_query("SELECT created
    FROM contest_entry
    WHERE ceid = :ceid", array(
    ':ceid' => $ceid,
    ))->fetchField();
  
  $log = array();
  $log[] = 'Set timestamp of entry ' . $ceid . ' to yesterday.';
  $log[] = 'Original: ' . ce_test_output_datetime($entry->created, 'east', CE_TEST_FORMAT_DAYTIME) . ' (eastern)';
  $log[] = 'Now set to: ' . ce_test_output_datetime($check, 'east', CE_TEST_FORMAT_DAYTIME) . ' (eastern)';
  
  $log = implode("<br/>\n");
  
  if (module_exists('devel')) {
    dsm($log);
  }
  
  return $log;
}
