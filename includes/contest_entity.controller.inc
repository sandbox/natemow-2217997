<?php
/**
 * @file
 * contest_entity.controller.inc
 */

class ContestType extends Entity {

  public function __construct(array $values = array(), $entityType = NULL) {

    global $user;

    $values += array(
      'is_new' => FALSE,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    parent::__construct($values, $entityType);
  }

  protected function defaultUri() {
    return array('path' => Contest::defaultUriAdmin() . '/types/manage/' . $this->identifier());
  }

  public function save() {

    $this->changed = REQUEST_TIME;

    return parent::save();
  }

}

class Contest extends Entity {

  public function __construct(array $values = array(), $entityType = NULL) {

    global $user;

    $values += array(
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    parent::__construct($values, $entityType);
  }

  protected function defaultUri() {
    return array('path' => 'contests/' . $this->identifier());
  }

  public static function defaultUriAdmin() {
    return 'admin/structure/contests';
  }

  public function save() {

    $this->changed = REQUEST_TIME;

    return parent::save();
  }

}

class ContestEntry extends Entity {

  public function __construct(array $values = array(), $entityType = NULL) {

    global $user;

    if (!empty($values['is_new'])) {
      $values += array(
        'uid' => $user->uid,
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'remote_addr' => ip_address(),
      );
    }
    else {
      $values['is_new'] = FALSE;
    }

    parent::__construct($values, $entityType);
  }

  protected function defaultUri() {
    return array('path' => 'contests/entry/' . $this->identifier());
  }

  public function save() {

    $this->changed = REQUEST_TIME;

    return parent::save();
  }

}

class ContestUIController extends EntityDefaultUIController {

  public function overviewFormSubmit($form, &$form_state) {

    // Update entity weight values.
    foreach ($form_state['values']['weight'] as $entity_id => $weight) {
      db_update('contest')
        ->fields(array(
          'weight' => $weight,
        ))
        ->condition('cid', $entity_id)
        ->execute();
    }

  }

  public function overviewForm($form, &$form_state) {

    $form = parent::overviewForm($form, $form_state);

    // The index position of our placeholder Weight column.
    $index_weight = 4;

    $form['table'] += array(
      '#empty' => t('No contests available.'),
      // Support for tabledrag's drag-and-drop feature.
      '#attributes' => array(
        'id' => 'contest-overview-table',
      ),
    );

    // Support for tabledrag's show/hide weights feature.
    $form['table']['#header'][$index_weight] = array(
      'data' => $form['table']['#header'][$index_weight],
      'class' => array('tabledrag-hide'),
    );

    $form['table']['weight']['#tree'] = TRUE;

    foreach ($form['table']['#rows'] as &$row) {
      // Intercept custom #entity data property prior to rendering.
      $entity = $row['#entity'];
      $entity_id = entity_id('contest', $entity);
      unset($row['#entity']);

      // Add the weight form element.
      $form['table']['weight'][$entity_id] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $entity->weight,
        '#delta' => count($form['table']['#rows']),
        // Support for tabledrag's drag-and-drop feature.
        '#attributes' => array(
          'class' => array('contest-weight'),
        ),
      );

      // Update weight column's content with ref to our new weight element.
      $row['data'][$index_weight]['data'] = &$form['table']['weight'][$entity_id];

      // Support for tabledrag's show/hide weights feature.
      $row['data'][$index_weight]['class'] = array('tabledrag-hide');

      // Support for tabledrag's drag-and-drop feature.
      $row['class'] = array('draggable');
    }

    // Initialize tabledrag.
    drupal_add_tabledrag('contest-overview-table', 'order', 'sibling', 'contest-weight');

    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 100,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;
  }

  public function overviewTable($conditions = array()) {

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // Add all conditions to query.
    foreach ($conditions as $key => $value) {
      $query->propertyCondition($key, $value);
    }
    // Sort the results by weight.
    $query->propertyOrderBy('weight');

    $results = $query->execute();
    $ids = isset($results[$this->entityType]) ? array_keys($results[$this->entityType]) : array();
    $entities = $ids ? entity_load($this->entityType, $ids) : array();

    $rows = array();
    foreach ($entities as $entity) {
      $rows[] = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity);
    }

    $render = array(
      '#theme' => 'table',
      '#header' => $this->overviewTableHeaders($conditions, $rows),
      '#rows' => $rows,
      '#empty' => t('None.'),
    );

    return $render;
  }

  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {

    $additional_header[] = t('Type');
    $additional_header[] = t('Mode');
    $additional_header[] = t('Limit');
    $additional_header[] = t('Weight');

    return parent::overviewTableHeaders($conditions, $rows, $additional_header);
  }

  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {

    $contest_options = _contest_form_options(FALSE);

    $label_type = $contest_options['type'][$entity->type];
    $label_mode = $contest_options['mode'][$entity->mode];
    $label_interval = $entity->limit_entry . ' ' . format_plural($entity->limit_entry, 'entry', 'entries') . ' ' . $contest_options['limit_interval'][$entity->limit_interval];
    if (empty($entity->limit_entry)) {
      $label_interval = t('unlimited');
    }

    $additional_cols[] = $label_type;
    $additional_cols[] = $label_mode;
    $additional_cols[] = $label_interval;
    $additional_cols[] = NULL;

    $row = parent::overviewTableRow($conditions, $id, $entity, $additional_cols);

    $row = array(
      'data' => $row,
      // Add a custom object to the return row for later use in
      // $this->overviewForm.
      '#entity' => &$entity,
    );

    return $row;
  }

}
